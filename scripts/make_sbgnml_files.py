#!python
import os.path
import subprocess

INPUT_DIR = "../maps/celldesigner/"
OUTPUT_DIR = "../maps/sbgnml/"
CD2SBGNML = "../third_party/cd2sbgnml/cd2sbgnml.sh"


def celldesigner_to_sbgnml(input_path, output_path):
    r = subprocess.run(
        [CD2SBGNML, input_path, output_path], capture_output=True
    )
    if r.returncode != 0:
        print(f"Error while converting {input_file} to SBGN-ML: {r.stderr}")


if __name__ == "__main__":
    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR, exist_ok=True)
    for input_file in os.listdir(INPUT_DIR):
        input_path = os.path.abspath(os.path.join(INPUT_DIR, input_file))
        output_file = input_file.split(".")[0] + ".sbgn"
        output_path = os.path.abspath(os.path.join(OUTPUT_DIR, output_file))
        celldesigner_to_sbgnml(input_path, output_path)
