#!python
import os.path
import sys

import stonpy

sys.path.insert(1, os.path.join(sys.path[0], ".."))
from credentials import *

ston = stonpy.core.STON(URI, USER, PASSWORD)
ston.graph.delete_all()

INPUT_DIR = "../maps/sbgnml/"
for input_file in os.listdir(INPUT_DIR):
    input_path = os.path.abspath(os.path.join(INPUT_DIR, input_file))
    ston.create_map(input_path, map_id=input_file)
