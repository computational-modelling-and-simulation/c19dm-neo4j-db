The C19DM-Neo4j database
------------------------

The database is accessible online at (https://c19dm-neo4j.lcsb.uni.lu/).
One can build the database locally using the input maps and scripts found in this repository.
The input maps are all stable versions of the maps that were available in the C19DM repository (https://git-r3lab.uni.lu/covid/models/) at the beginning of November 2020 (commit a705765a).

Requirements
------------

- Neo4j >= 4.2 with APOC installed (https://neo4j.com/labs/apoc/)
- StonPy (https://github.com/adrienrougny/stonpy/)

Build the SBGN-ML files
-----------------------

The input maps are in the CellDesigner format.
They must be converted to the SBGN-ML format before being stored in a Neo4j database.
To convert the maps to the SBGN-ML format:

- Run the script `scripts/make_sbgnml_files.py` from the `scripts` directory: `cd scripts/; ./make_sbgnml_files.py`

Populate a local Neo4j database
-------------------------------

To populate a local Neo4j database with the maps:

- Start Neo4j
- Change the login credentials in file `credentials.py` to those of your local Neo4j database
- Run the `scripts/create_db.py` script from the `scripts/` directory: `cd scripts/; ./create_db.py`

License
---------

The scripts offered in the `scripts/` directory are licensed under the GPLv3.
The `scripts/make_sbgnml_files.py` uses the third party CD2SBGNML tool (Balaur et al., 2020).
This tool is made available here in the `third_party/cd2sbgnml` directory.
It is licensed under the LGPL3 (see `third_party/cd2sbgnml/LICENSE`).
